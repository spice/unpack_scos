#define MERGE_MISSING_LIMIT 1200
// in sec: merge 2 edds requests if there are less than MERGE_MISSING_LIMIT seconds
// between the current start and the previous end

// Those structures allows to loop the gap separately on the apid 
typedef struct {
    int apid;
    time_t start_sec;
    time_t end_sec;
} Gap; // Gap: everything needed to print the EDDS request

typedef struct {
	Gap* gaps;
	int nb_gaps;

	int* apids; // apid array
	int nb_apids;
} Missings;

void print_one_request(Gap gap);
void print_edds_requests(Gap gap);
int read_packet(char* packet_str, Missings* missings,char * name_file,FILE* missing_file_log,int argc,char** argv);
int check_ssc(int apid, int ssc, struct tm * ts, long int sec_pkt, Missings* missings,char * name_file,int type,int subtype, FILE* missing_file_log,int argc,char** argv);
void add_missing(Missings* missings, int apid, time_t start_sec, time_t end_sec);
