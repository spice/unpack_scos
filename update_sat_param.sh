#!/bin/bash
set -x
# Load the last SAT params into the MISO database
# Give a YYYYMMDD date in argument to load this date, otherwise it will look for the day "today - 1"

used_server='spice-stable'
#root_path='/home/david/projects/unpack_scos/'
root_path='/home/edds/unpack_scos/'
params='NPWT2519,NPWT2503,NTCD50L6,NTCD4ZZ6,NRU02615,NRU02609,NCFT1C80'
apids='212,260'


cd $root_path'../edds_client'

if [ -n "$1" ]
then
	date -d $1 +'%Y%m%d' >/dev/null 2>&1
	exit_=false

	if [ ! $? -eq 0 ]
	then
		exit_=true
	fi
	if [[ $1 == *"-"* ]] # contains dashes
	then
		exit_=true
	fi

	if [ $exit_ = true ]
	then
		echo 'The argument has to be in the YYYYMMDD format (without dashes).'
		exit
	fi

	startReq=$1
else
	startReq=$(date -d 'today - 1 day' +'%Y%m%d')
fi

endReq=$(date +%Y%m%d -d $startReq' + 1 day')

# define filename for the answer
JOUR_QUERY=$(date -d $startReq '+%Y_%m_%dT%H_%M')
echo date du jour $JOUR_QUERY

# before querying remove existing file to avoid duplicates
rm -f /home/edds/edds_response/SAT_${JOUR_QUERY}_Batch*

python3 -m edds_client.cli --config ../spice_config.json -a $apids -f prefix -r SAT_$JOUR_QUERY -s $startReq'T00:00:00Z' -e $endReq'T00:00:00Z' --data_type Param --param_filter $params --data_space DARC -g

# one day usually takes about 5 minutes, wait a few sec after the command ends
sleep 5

# the output file will be named like
# SAT_$JOUR_QUERY.... .xml

AN=${JOUR_QUERY:0:4}
MO=${JOUR_QUERY:5:2}
JO=${JOUR_QUERY:8:2}

file_answer=`ls /home/edds/edds_response/SAT_${JOUR_QUERY}_Batch* | sort`
# there may be several gile in the answer of EDDS, they are numbered 1,2,3 ...
for file in $file_answer
do
    number_string=`echo $file | rev | cut -d'.' -f 2 | rev `
    number=$((number_string))
    if (($number > 1))
    then
	# append to first file
	python3 $root_path/merge_param_sat_file.py $file sat_${AN}_${MO}_${JO}.xml
    else
	cp $file ./sat_${AN}_${MO}_${JO}.xml
    fi
done
mkdir -p /telemetry/$AN/$MO/$JO
mv ./sat_${AN}_${MO}_${JO}.xml /telemetry/$AN/$MO/$JO/sat_${AN}_${MO}_${JO}.xml


