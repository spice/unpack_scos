#!/bin/bash

#set -x

last_missing_run_file=/home/edds/missing_monit_tab/last_missing_run

if [ -e "$last_missing_run_file" ]
then
    last_run=`cat $last_missing_run_file`
else
    last_run="2020-02-23T18:45:03"
fi

# keep run date for next time
run_date=$(date '+%Y-%m-%dT%H:%M:%S')

echo $run_date > $last_missing_run_file

log_file_date=$(date +%Y%m%d)

# hk, cms,sc,other

for file in `find /home/telemetry -type f -newermt $last_run -name '*_*.xml' | sort`
do
    #echo $file
    bn_file=`basename $file`
    echo $bn_file
    # lets take the file before and after to check holes
    if [[ $bn_file =~ (.*)_(.*)_(.*)_(.*)\.xml$ ]]; then
	typ=${BASH_REMATCH[1]}
	AN=${BASH_REMATCH[2]}
	MO=${BASH_REMATCH[3]}
	JO=${BASH_REMATCH[4]}
    fi
    if [ $typ == "sat" ]; then
	continue
    fi
    dd=$AN-$MO-$JO
    dd_before=$dd
    dd_after=$dd
    # find file around this one, may be a few days appart
    while true
    do
	before=$(date --date="${dd_before} -1 day" +%Y_%m_%d)
	AN_before=$(date --date="${dd_before} -1 day" +%Y)
	MO_before=$(date --date="${dd_before} -1 day" +%m)
	JO_before=$(date --date="${dd_before} -1 day" +%d)
	before_file=/home/telemetry/$AN_before/$MO_before/$JO_before/${typ}_$before.xml
	[ -f "$before_file" ] && break
	dd_before=$(date --date="${dd_before} -1 day" +%Y-%m-%d)
	debut=$(date --date 2020-02-23 +%s)
	current=$(date --date "${dd_before}" +%s)
	if [ $debut -ge $current ];
	then
	    break
	fi
    done
    while true
    do
	after=$(date --date="${dd} +1 day" +%Y_%m_%d)
	AN_after=$(date --date="${dd} +1 day" +%Y)
	MO_after=$(date --date="${dd} +1 day" +%m)
	JO_after=$(date --date="${dd} +1 day" +%d)
	after_file=/home/telemetry/$AN_after/$MO_after/$JO_after/${typ}_$after.xml
	[ -f "$after_file" ] && break
	dd_after=$(date --date="${dd_after} +1 day" +%Y-%m-%d)
	today=$(date +%s)
	current=$(date --date "${dd_after}" +%s)
	if [ $today -le $current ];
	then
	    break
	fi
    done
    # merge xml files
    mkdir -p tempmissing
    rm -rf ./tempmissing/*
    [ -f "$before_file" ] && cp $before_file tempmissing/.
    [ -f "$after_file" ] && cp $after_file tempmissing/.
    cp $file tempmissing/.
    rm -f ./${typ}_${dd}.xml
    /opt/merge_xml/merge_xml tempmissing ${typ}_${dd}.xml
    /home/edds/unpack_scos/unpack_scos ${typ}_${dd}.xml -missing missing_${log_file_date}.log
    rm -rf ./tempmissing
    rm -f ./${typ}_${dd}.xml
done


