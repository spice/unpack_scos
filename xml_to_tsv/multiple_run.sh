#!/bin/bash

# ls to_run and save in output_tsv/extra_params1,2,3...txt (1 input = 1 output)

#root_path='/home/david/projects/unpack_scos/'
root_path='/home/edds/unpack_scos/'

to_run='extra_decoded_params_wa'
cd $root_path'xml_to_tsv'
rm -f output_tsv/*
i=1
for xml_file in `ls $root_path'/'$to_run`
do
	cp extra_params_empty.txt output_tsv/extra_params.txt
	echo '> '$xml_file' converting to output_tsv/extra_params'$i'.txt'
    ./xml_to_tsv $root_path'/'$to_run'/'$xml_file
    cp 'output_tsv/extra_params.txt' 'output_tsv/extra_params'$i'.txt'
    i=$((i+1))
done
