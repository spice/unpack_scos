#include "head.h"

int i, j, k;
Parameter* parameters;
char output_path[] = "output_tsv/extra_params.txt";
char *param_names[] = {"NPWT2519", "NPWT2503", "NTCD50L6", "NTCD4ZZ6", "NRU02615", "NRU02609", "NCFT1C80"};
// RAL asked for those ones
int nb_params = sizeof(param_names) / sizeof(param_names[0]);

int is_regular_file(const char *path)
{
	struct stat path_stat;
	stat(path, &path_stat);
	return S_ISREG(path_stat.st_mode);
}

void init_parameters()
{
	parameters = (Parameter*)malloc(sizeof(Parameter) * nb_params);
	for (i = 0; i < nb_params; i++)
	{
		(parameters[i]).column_number = i + 4;
		(parameters[i]).name = (char*)malloc(sizeof(char) * PARAM_NAME_LENGTH);
		strcpy((parameters[i]).name, param_names[i]);
		(parameters[i]).use_eng = (i != 6); // NCFT1C80 raw
		(parameters[i]).values = NULL;
		(parameters[i]).nb_values = 0;
	}
}

Parameter* get_parameter(char* param_name)
{
	for (i = 0; i < nb_params; i++)
	{
		if (strcmp((parameters[i]).name, param_name) == 0)
			return &(parameters[i]);
	}

	return NULL;
}

void push_value(Parameter* current_parameter, double date_sec, double value)
{
	ParameterValue* new_values = (ParameterValue*)malloc(sizeof(ParameterValue) * (++(current_parameter->nb_values)));
	
	for (i = 0; i < current_parameter->nb_values; i++)
	{
		if (i < current_parameter->nb_values - 1)
		{
			(new_values[i]).date_sec = ((current_parameter->values)[i]).date_sec;
			(new_values[i]).value = ((current_parameter->values)[i]).value;
		}
		else
		{
			(new_values[i]).date_sec = date_sec;
			(new_values[i]).value = value;
		}
	}

	free(current_parameter->values);
	current_parameter->values = new_values;
}

void fill_parameter_values(char* file_path)
{
	xmlDoc *document;
	xmlNode *root, *node, *response, *pkt_list, *pkt, *raw_pkt, *element_type;
	Parameter* current_parameter;
	char current_date[100];
	char eng_value[100];
	char raw_value[100];
	double value;
	double date_sec;
	int length, _pos;

	// ex: 2020-04-15T10:30:58.408709
	char datetime_str[100]; // 2020-04-15T10:30:58
	char datetime_rest[20]; // 408709
	char datetime_rest_decimal[23]; // 0.408709
	struct tm ts;
	mktime(&ts); // FIXME malloc aborted without this line??

	document = xmlReadFile(file_path, NULL, 0);
	root = xmlDocGetRootElement(document);
	for (node = root->children; node; node = node->next)
	{
		if (strcmp((char*)(node->name), "Response") == 0)
		{
			response = node;
			for (pkt_list = response->children; pkt_list; pkt_list = pkt_list->next)
			{
				if (strcmp((char*)(pkt_list->name), "ParamResponse") == 0)
				{
					pkt = pkt_list;
					for (pkt = pkt_list->children; pkt; pkt = pkt->next)
					{
						for (raw_pkt = pkt->children; raw_pkt; raw_pkt = raw_pkt->next)
						{
							if (strcmp((char*)(raw_pkt->name), "ParamSampleListElement") == 0)
							{
								for (element_type = raw_pkt->children; element_type; element_type = element_type->next)
								{ // element_type are TimeStampAsciiA, Name, RawValue...
									if (strcmp((char*)(element_type->name), "Name") == 0)
										current_parameter = get_parameter((char*)xmlNodeGetContent(element_type));
									if (strcmp((char*)(element_type->name), "TimeStampAsciiA") == 0)
										strcpy(current_date, (char*)xmlNodeGetContent(element_type));
									if (strcmp((char*)(element_type->name), "EngineeringValue") == 0)
										strcpy(eng_value, (char*)xmlNodeGetContent(element_type));
									if (strcmp((char*)(element_type->name), "RawValue") == 0)
										strcpy(raw_value, (char*)xmlNodeGetContent(element_type));
								}
								
								// Reconstruct date:
								_pos = strcspn(current_date, ".");

								length = _pos;
								memcpy(datetime_str, current_date, length);
								datetime_str[length] = '\0';
								strptime(datetime_str, "%Y-%m-%dT%H:%M:%S", &ts);
								date_sec = (double)mktime(&ts);

								length = strlen(current_date) - _pos + 1;
								memcpy(datetime_rest, &current_date[_pos + 1], length);
								datetime_rest[length] = '\0';
								sprintf(datetime_rest_decimal, "0.%s", datetime_rest);
								date_sec += atof(datetime_rest_decimal);
								// ---
								
								value = atof(current_parameter->use_eng ? eng_value : raw_value);
								push_value(current_parameter, date_sec, value);
							}
						}
					}
				}
			}
		}
	}
}

void write_parameter_values()
{
	Parameter param;
	ParameterValue paramValue;
	FILE* fp;
	char* line_part = (char*)malloc(sizeof(char) * (nb_params + 1) * VALUE_LENGTH);
	char* value_str = (char*)malloc(sizeof(char) * VALUE_LENGTH); // (with tab the first char)

	fp = fopen(output_path, "a");
	if (fp == NULL)
	{
		printf("File not found, %s\n", output_path);
		return;
	}

	for (i = 0; i < nb_params; i++)
	{
		param = parameters[i];
		for (j = 0; j < (parameters[i]).nb_values; j++)
		{
			paramValue = ((parameters[i]).values)[j];
			line_part[0] = '\0';
			for (k = 4; k < param.column_number; k++)
				strcat(line_part, "\t-");
			if (param.use_eng)
				sprintf(value_str, "\t%.15lf", paramValue.value);
			else // raw are integers
				sprintf(value_str, "\t%.0lf", paramValue.value);
			strcat(line_part, value_str);
			for (k = 0; k < 3 + nb_params - param.column_number; k++)
				strcat(line_part, "\t-");

			fprintf(fp, "\n%.6lf\t0\t0%s", paramValue.date_sec, line_part);
		}
	}

	fclose(fp);
}

int main(int argc, char** argv)
{
	// Checks
	if (argc <= 1)
	{
		fprintf(stderr, "ERROR: Give in argument the file.xml or a folder which contains xml files.\n");
		return EXIT_FAILURE;
	}

	char* input_path = (char*)malloc(sizeof(char) * (strlen(argv[1]) + 1)); // can be a file or a folder
	strcpy(input_path, argv[1]);

	if (access(input_path, F_OK) == -1)
	{
		fprintf(stderr, "ERROR: The file or folder given in argument doesn't exist.\n");
		return EXIT_FAILURE;
	}

	short int recursive;
	struct dirent *dp;
	DIR *dfd;
	char filename_qfd[1000];

	recursive = !is_regular_file(input_path); // we suppose it's a directory if the user didn't give a file

	init_parameters();

	if (recursive)
	{
		if ((dfd = opendir(input_path)) == NULL)
		{
			fprintf(stderr, "ERROR: Can't open the directory: %s\n", input_path);
			return EXIT_FAILURE;
		}

		while ((dp = readdir(dfd)) != NULL)
		{
			struct stat stbuf;
			sprintf(filename_qfd, "%s/%s", input_path, dp->d_name);
			if (stat(filename_qfd, &stbuf) == -1)
			{
				printf("Warning: Unable to stat file: %s\n", filename_qfd);
				continue;
			}

			if ((stbuf.st_mode & S_IFMT ) == S_IFDIR)
				continue; // Skip directories
			else
				fill_parameter_values(filename_qfd);
		}
	}
	else
		fill_parameter_values(input_path);

	
	write_parameter_values();

	printf("\n");
	return EXIT_SUCCESS;
}
