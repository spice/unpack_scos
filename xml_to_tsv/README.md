(Don't use it yet, it's a work in progess program)

Convert a XML file (decoded packet from EDDS request) to a tab separated values (TSV) file, readable by MISO.  
> **`make`  
`cp extra_params_empty.txt output_tsv/extra_params.txt`
`./xml_to_tsv file_or_folder`**

file_or_folder is a file.xml or a folder which contains the XML files.  
The generated TSV files will be in output_tsv

If you don't do the copy first, it will append in output_tsv/extra_params.txt

Example:  
**`./xml_to_tsv input_examples/part.xml`**

