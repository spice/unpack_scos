#ifndef HEAD_H

	#define HEAD_H
	#define __USE_XOPEN
	#define _GNU_SOURCE

	#include <stdlib.h>
	#include <stdio.h>
	#include <string.h>
	#include <unistd.h>
	#include <dirent.h>
	#include <sys/types.h>
	#include <sys/stat.h>
	#include <time.h>
	#include <libxml/parser.h>

	#define PARAM_NAME_LENGTH 9 // 8 + '\0'
	#define VALUE_LENGTH 20 // in output (raw, eng value)

	typedef struct {
		double date_sec;
		double value;
	} ParameterValue;

	typedef struct {
		int column_number; // starts to 4th col in the file (firsts are date, cuc, scc)
		char* name;
		short int use_eng; // 1: engineering value used, 0: raw one

		ParameterValue* values;
		int nb_values;
	} Parameter;

	int is_regular_file(const char *path);
	void init_parameters();
	Parameter* get_parameter(char* param_name);
	void push_value(Parameter* current_parameter, double date_sec, double value);
	void fill_parameter_values(char* file_path);
	void write_parameter_values();

#endif
