#!/bin/bash

# injest a file which contains the edds request (duplicated is fine, they will be removed)

#root_path='/home/david/projects/unpack_scos/'
root_path='/home/edds/unpack_scos/'
log_path=$root_path'missing_logs'
regenerated_path=$root_path'../edds_response'
edds_contains='/home/edds/.local/bin/edds'
edds_prefix='missing_packets' # instead of "retriv": allows to identify the regenerated edds packets here

currentDate=$(date '+%Y-%m-%d_%Hh%Mm%Ss')

was_run_subfolder='manually'

if [ -n "$1" ]
then
	if [ -n "$2" ]
	then
		was_run_subfolder=$2
	fi

	> $log_path"/was_run/"$was_run_subfolder"/"$currentDate"_commands.log"
	> $log_path"/was_run/"$was_run_subfolder"/"$currentDate"_insert_xml_result.log"

	resp_path=$log_path'/response_wd.log'
	cat $1 | uniq -u > $resp_path 2>&1 # without duplicates
	sed -i 's/retriv/'$edds_prefix'/g' $resp_path
	
	old_nb_lines=$(cat $1 | wc -l)
	new_nb_lines=$(cat $resp_path | wc -l)
	nb_duplicated=$(($old_nb_lines - $new_nb_lines))
	echo -e '('$nb_duplicated' duplicated lines detected and removed)'
	
	nb_requests=0

	cat $resp_path | ( while read edds_request 
	do
		if [[ $edds_request == $edds_contains* ]]
		then
			cd $root_path'../'
			IFS=' ' read -r -a array <<< "$edds_request"
			apid=${array[2]}
			echo APID $apid
			case $apid in
			'1364' | '1367' | '1380' | '1396' | '1432')
				insert_prefix='hk'
				;;
			'1388' | '1404' | '1644' | '1628')
				insert_prefix='sc'
				;;
			'1667')
				insert_prefix='cms'
				;;
			'796' | '1363' | '1369' | '1377' | '1383' | '1384' | '1385' | '1412' | '1416')
				insert_prefix='other'
				;;
			'1420' | '1436')
				insert_prefix='ll'
				;;
			*)
				echo 'APID not handled.'
				exit
				;;
			esac

			if [ $nb_requests -gt 0 ]
			then
				echo -e '\nWaiting 5min before each request...'
				sleep 300 # Sleep 5min before running another request
			fi

			$edds_request >> $log_path"/was_run/"$was_run_subfolder"/"$currentDate"_edds.log" 2>&1
			echo $edds_request >> $log_path"/was_run/"$was_run_subfolder"/"$currentDate"_commands.log" 2>&1
			ls_cmd='ls '$regenerated_path'/'$edds_prefix'*'
			nb_requests=$((nb_requests + 1))
			
			$ls_cmd
			if [ $? -eq 0 ]
			then
				for edds_file in `$ls_cmd`
				do
					/opt/merge_xml/insert_xml $edds_file $insert_prefix >> $log_path"/was_run/"$was_run_subfolder"/"$currentDate"_insert_xml_result.log" 2>&1
					echo '/opt/merge_xml/insert_xml '$edds_file $insert_prefix' >> '$log_path"/was_run/"$was_run_subfolder"/"$currentDate"_insert_xml_result.log 2>&1" >> $log_path"/was_run/"$was_run_subfolder"/"$currentDate"_commands.log" 2>&1
					mv $edds_file $log_path"/was_run/responses/"
					echo 'mv '$edds_file' '$log_path'/was_run/responses/' >> $log_path"/was_run/"$was_run_subfolder"/"$currentDate"_commands.log" 2>&1
				done
			fi
		else
			echo 'A line was ignored: not an EDDS request.'
		fi
	done

	echo '' # line break

	if [ $nb_requests -gt 1 ]
	then
		echo $nb_requests' requests were run.'
	else
		echo $nb_requests' request was run.'
	fi ) # close "( while". Without those parenthesis nb_requests = 0
	
	rm -f $resp_path
	
	echo 'See the run commands in: '$log_path'/was_run/'$was_run_subfolder'/'$currentDate'_commands'.log
else
	echo -e 'You have to specify the input (has to contain edds requests): ./process_missing_packets.sh requests.log'
fi
