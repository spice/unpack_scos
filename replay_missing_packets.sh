#!/bin/bash

#root_path='/home/david/projects/unpack_scos/'
root_path='/home/edds/unpack_scos/'

if [ -n "$1" ]
then
	date -d $1 +'%Y-%m-%d' >/dev/null 2>&1
	if [ ! $? -eq 0 ]
	then
		echo 'The argument has to be in the YYYY-MM-DD format.'
		exit
	fi

	start_date=$1
else
	rm -f $root_path'/missing_logs/logs_replay/*'
	start_date='2020-02-02' # day - 4 gives "2020-01-29": the first date with telemetry in /home/edds/edds_response
fi

today_str=$(date '+%Y-%m-%d')

current_date=$start_date
while [ $current_date != $today_str ]
do
	$root_path'check_missing_packets.sh' $current_date replay
	current_date=$(date +%Y-%m-%d -d $current_date' + 1 day')
done
