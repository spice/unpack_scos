import sys
import xml.etree.ElementTree as ET

# arg1 to merge into arg2

print (sys.argv)

if len(sys.argv) != 3:
    print("Usage: merge_param_sat_file.py file_to_append file_result")
    exit(1)

file = sys.argv[1]
result = sys.argv[2]
print (file, result)

tree = ET.parse(file)
root = tree.getroot()

tree_res = ET.parse(result)
root_result = tree_res.getroot()

for el in root_result:
    for el1 in el:
        for el2 in el1:
            if el2.tag == 'ParamSampleList':
                res_param_sample_list = el2


for elem in root:
    for subelem in elem:
        for subsub in subelem:
            if subsub.tag == 'ParamSampleList':
                param_sample_list = subsub
                for param in param_sample_list:
                    res_param_sample_list.append(param)

tree_res.write(result)

