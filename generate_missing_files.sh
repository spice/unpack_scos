#!/bin/bash

# missing files from unpack_scos -missing for histogram missing tab (TM monitor)
# /!\ Run this from edds@gfts

cd /home/edds/unpack_scos

for file in `ls /telemetry/*/*/*/*.xml`
do
    ./unpack_scos "$file" -missing
done
