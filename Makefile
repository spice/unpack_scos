CFLAGS = -fPIC -W -Wall -g -I /usr/include/libxml2
unpack_scos: main.o
	gcc $(CFLAGS) -o unpack_scos main.o -lxml2

main.o: main.c
	gcc $(CFLAGS) -o main.o -c main.c -W -Wall -pedantic

clean:
	rm -rf *.o
