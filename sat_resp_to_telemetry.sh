#!/bin/bash
set -x

# Give a YYYYMMDD date in argument to load this date

root_path='/home/edds/unpack_scos/'

if [ -n "$1" ]
then
	date -d $1 +'%Y%m%d' >/dev/null 2>&1
	exit_=false

	if [ ! $? -eq 0 ]
	then
		exit_=true
	fi
	if [[ $1 == *"-"* ]] # contains dashes
	then
		exit_=true
	fi

	if [ $exit_ = true ]
	then
		echo 'The argument has to be in the YYYYMMDD format (without dashes).'
		exit
	fi

	startReq=$1
else
    echo 'The argument has to be in the YYYYMMDD format (without dashes).'
fi

endReq=$(date +%Y%m%d -d $startReq' + 1 day')

# define filename for the answer
JOUR_QUERY=$(date -d $startReq '+%Y_%m_%dT%H_%M')
echo date du jour $JOUR_QUERY

# the output file will be named like
# SAT_$JOUR_QUERY.... .xml

AN=${JOUR_QUERY:0:4}
MO=${JOUR_QUERY:5:2}
JO=${JOUR_QUERY:8:2}

file_answer=`ls /home/edds/edds_response/SAT_${JOUR_QUERY}_Batch* | sort`
# there may be several gile in the answer of EDDS, they are numbered 1,2,3 ...
for file in $file_answer
do
    number_string=`echo $file | rev | cut -d'.' -f 2 | rev `
    number=$((number_string))
    if (($number > 1))
    then
	# append to first file
	python3 $root_path/merge_param_sat_file.py $file sat_${AN}_${MO}_${JO}.xml
    else
	cp $file ./sat_${AN}_${MO}_${JO}.xml
    fi
done
mkdir -p /telemetry/$AN/$MO/$JO/
mv ./sat_${AN}_${MO}_${JO}.xml /telemetry/$AN/$MO/$JO/sat_${AN}_${MO}_${JO}.xml


