#!/bin/bash
# If only one argument is given, only an "echo" is perfomed which allows to check if the insert_xml lines are the ones we want
# This first argument is an integer and answsers to how many days in the past we need to look for
# When ready run this script again with a second argument like ./insert_many.sh 17 go

# Edit this if needed
insert_prefix='sc' # prefixes are cms, hk, sc, ll, other
edds_prefix='SCI' # in edds_response

if [ -n "$1" ]
then
    nb_days=$1
else
    echo 'Give the number of days in the past you want to look for.'
    exit 1
fi

cd /home/edds

for edds_file in $(find edds_response/ -mtime -$nb_days -name "$edds_prefix"* | sort)
do
    if [ -n "$2" ]
    then
        echo "Running insert_xml..."
        /opt/merge_xml/insert_xml $edds_file $insert_prefix
    else
        echo "/opt/merge_xml/insert_xml $edds_file $insert_prefix"
    fi
done

if [ -n "$2" ]
then
    echo -e "\ninsert_xml lines were performed, check the /telemetry content with\nls -ltr /telemetry/*/*/*/$insert_prefix*"
else
    echo -e "\nTo run those lines for real, add a second argument like: ./insert_many.sh $nb_days go"
fi
