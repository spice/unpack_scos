# Decode the SCOS Header from the EDDS xml packet file and print date apid type and  sub type
Allows to quickly check the contents of a file 

## 1) Download the project:
```
cd somewhere/on_your/disk/my_projects
git clone https://git.ias.u-psud.fr/spice/unpack_scos.git
```

## 2) Add libxml dependency if necessary
```
ls /usr/include/libxml2
```
If needed:
```
sudo apt-get update
sudo apt-get install libxml2-dev
```

## 3) Make and run:
```
cd unpack_scos
chmod +x run.sh
./run.sh path_to_your_file/the_packet_file.xml
```
A working example:
```
./run.sh examples/BatchRequest.PktTmRaw.SOL.0.2019.177.15.32.22.137.VsXM@2019.177.15.32.23.349.1.xml
```

## Note 1:
The results are separated by tabs and show:\
packet_number, scos_packet_date, PUS packet date , APID, Type and Subtype

Whenever ssc are not increasing by one -> MISSING packet message
and print on stderr the commande to send to edds to retreive missing

## Note 2:
After the first use, you can direclty call the program again with:
```
./unpack_scos another_file.xml
```
check_continu.sh -> fetch all file from /telemetry to check ssc increase

## Note 3:

scripts:
update_sat_param.sh - run query for 7 interesting parameters, with generation dates, for one day ONLY.

it returns an xml with a spcific format, xml which it eventually merge if sevral file are return
and stored into /telemetry/YYYY/MM/DD/sat_2021_01_25.xml

in order to leep the data unique, and with the rigth date, rm in edds_response is done for the query day


./update_several_sat_params.sh 20210101 20210228
calls the above fr each day

sat_resp_to_telemetry.sh takes the parameters from edds_response to avoid to redo the queries to EDDS but take
the answers and mv them to /telemetry

FYI: once the paramaters are stored in telemetry, a cron takes them from spice-monit to store them
time order and compacted into .txt for the monitoring (run_sat.sh)