rm -f file_list.txt
for file in `ls /telemetry/*/*/*/*.xml`
do
    fic=`basename $file`
    echo $fic >> file_list.txt
done

sort < file_list.txt > file_list_sorted.txt



for fic in `cat file_list_sorted.txt`
do
    echo $fic and previous $last_file
    pref1=`echo $fic | cut -d '_' -f 1`
    pref2=`echo $last_file | cut -d '_' -f 1`
    an1=`echo $fic | cut -d '_' -f 2`
    an2=`echo $last_file | cut -d '_' -f 2`
    m1=`echo $fic | cut -d '_' -f 3`
    m2=`echo $last_file | cut -d '_' -f 3`
    j1=`basename -s .xml $fic | cut -d '_' -f 4`
    j2=`basename -s .xml $last_file | cut -d '_' -f 4`
    if [ "$pref1" = "$pref2" ]
    then
	rm -rf temp fichier_2j.xml
	mkdir temp
	echo checking $last_file and $fic
	cp /telemetry/$an2/$m2/$j2/$last_file temp/.
	cp /telemetry/$an1/$m1/$j1/$fic temp/.
	/opt/merge_xml/merge_xml temp fichier_2j.xml
	./unpack_scos fichier_2j.xml > trace2j_${pref1}_${an1}_${m1}_${j1} 
    else
	./unpack_scos /telemetry/$an1/$m1/$j1/$fic > trace_${pref1}__${an1}_${m1}_${j1} 
    fi
    last_file=$fic
done


