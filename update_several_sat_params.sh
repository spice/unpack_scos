#!/bin/bash

# Run several times update_sat_param.sh from days "start" to "end"
# Use: ./update_several_sat_params.sh start [end]
# start and end must be in YYYYMMDD format, if end is not given, today is used instead

if [ ! -n "$1" ]
then
	echo -e 'Give at least one argument:\n./update_several_sat_params.sh start\nor\n./update_several_sat_params.sh start end\n(start and end must be YYYYMMDD format without dashes) '
	exit 1
fi

date -d $1 +'%Y%m%d' >/dev/null 2>&1
if [ ! $? -eq 0 ]
then
	echo 'Wrong date format, use YYYYMMDD without dashes.'
	exit 1
fi

end_day=$(date -d 'today' +'%Y%m%d')
if [ -n "$2" ]
then
	date -d $2 +'%Y%m%d' >/dev/null 2>&1
	if [ ! $? -eq 0 ]
	then
		echo 'Wrong date format, use YYYYMMDD without dashes.'
		exit 1
	fi
	end_day=$2
fi

next_day=$1
continue_=true
while [ $continue_ = true ]
do
    ./update_sat_param.sh $next_day
    if [ "$next_day" = "$end_day" ]
    then
    	continue_=false
    fi
    next_day=$(date +%Y%m%d -d $next_day' + 1 day')
done
