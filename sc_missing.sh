# from april2, to may-8
START="20220402 02:00:00"
STOP="20220508 00:00:01"
stop_sec=$(date -d "$STOP" +%s)

# SCI (APIDs 1388, 1404, 1644, 1628)

while true
do
    END=$(date -d "$START +hour" +"%Y%m%d %H:%M:%S")
    END=$(date -d "$END +hour" +"%Y%m%d %H:%M:%S")
    DEBUT=$(date -d "$START"  +"%Y%m%dT%H:%M:%SZ")
    FIN=$(date -d "$END" +"%Y%m%dT%H:%M:%SZ")
    echo /home/edds/.local/bin/edds -a \'1388, 1404, 1644, 1628\' -s $DEBUT -e $FIN --config ./spice_config.json -f prefix -r allsc -q -b PARC_FLIGHT_SOL -g
    echo sleep 300
    end_sec=$(date -d "$END" +%s)
    if [ $end_sec -ge $stop_sec ];
    then
	break
    fi
    START=$(date -d "$START +hour" +"%Y%m%d %H:%M:%S")
    START=$(date -d "$START +hour" +"%Y%m%d %H:%M:%S")
done
#/home/edds/.local/bin/edds -a 1667 -s  20200713T00:00:00Z -e 20200714T00:00:00Z --config ./spice_config.json -f prefix -r retriv -q -b PARC_FLIGHT_SOL -g
