#define __USE_XOPEN
#define _GNU_SOURCE

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <libxml/parser.h>
#include <libgen.h>

#include "header.h"

void print_one_request(Gap gap)
{
    struct tm ts_start;
    struct tm ts_end;
    char start_str[50];
    char end_str[50];
    char format[50];

    strcpy(format, "%Y%m%dT%H:%M:%SZ");

    time_t offset_sec = 120; // will look at 120 sec before and after the found dates
    time_t start_s = gap.start_sec - offset_sec;
    time_t end_s = gap.end_sec + offset_sec;

    ts_start = *gmtime(&start_s);
    ts_end = *gmtime(&end_s);
    strftime(start_str, sizeof(start_str), format, &ts_start);
    strftime(end_str, sizeof(end_str), format, &ts_end);

    fprintf(stderr, "/home/edds/.local/bin/edds -a %d -s %s -e %s --config ./spice_config.json -f prefix -r retriv -q -b PARC_FLIGHT_SOL -g\n", gap.apid, start_str, end_str);
}

void print_edds_requests(Gap gap)
{
    if (gap.start_sec == 0)
        return;
    
    int maxInSec = 3600*24; // split requests to have 1 day max
    time_t end_save = gap.end_sec;
    short int too_large = 0;

    if (gap.end_sec - gap.start_sec > maxInSec)
        too_large = 1;

    // Split too large requests
    while (too_large) // actually a while(true) only when request is too large
    {
        gap.end_sec = gap.start_sec + maxInSec;
        if (gap.end_sec >= end_save)
        {
            gap.end_sec = end_save;
            break;
        }
        print_one_request(gap);
        gap.start_sec += maxInSec;
    }

    print_one_request(gap);
}

void add_missing(Missings* missings, int apid, time_t start_sec, time_t end_sec)
{
    Gap* new_gaps = (Gap*)malloc(sizeof(Gap) * (++(missings->nb_gaps)));
    int i;
    for (i = 0; i < missings->nb_gaps; i++)
    {
        if (i < missings->nb_gaps - 1)
        {
            (new_gaps[i]).apid = ((missings->gaps)[i]).apid;
            (new_gaps[i]).start_sec = ((missings->gaps)[i]).start_sec;
            (new_gaps[i]).end_sec = ((missings->gaps)[i]).end_sec;
        }
        else // last iterance: save the new packet
        {
            (new_gaps[i]).apid = apid;
            (new_gaps[i]).start_sec = start_sec;
            (new_gaps[i]).end_sec = end_sec;
        }
    }
    
    free(missings->gaps);
    missings->gaps = new_gaps;

    // Add apid in indexes if not done yet
    int iterated_apid = 0;
    int* new_apids = (int*)malloc(sizeof(int) * (++(missings->nb_apids)));
    for (i = 0; i < missings->nb_apids; i++)
    {
        if (i < missings->nb_apids - 1)
        {
            iterated_apid = (missings->apids)[i];
            if (iterated_apid == apid)
                break;
            new_apids[i] = iterated_apid;
        }
        else
            new_apids[i] = apid;
    }

    if (i < missings->nb_apids - 1) // break was triggered: apid already stored => process canceled
    {
        --(missings->nb_apids);
        free(new_apids);
    }
    else // here i == missings->nb_apids
    {
        free(missings->apids);
        missings->apids = new_apids;
    }
}

/* cf 
Solar Orbiter Data Delivery Interface Control Document
'SOL-ESC-IF-05011,Is.1,Rev.2.20190626 (SOL DDID)_signed_PM.pdf'

first common TC/TM header 60 bytes, ie. 120 char in the hexa char xml file from edds
for TM another 16 bytes, ie. 32 char in the hexa file xml file from edds

whatever is after is PUS application packet

*/
int check_ssc(int apid, int ssc, struct tm * ts, long int sec_pkt, Missings* missings,char* name_file,int type,int subtype,FILE* file_log_unfound,int argc,char** argv) {

    #define NB_APID 50

    static int apid_ssc[NB_APID][3]; // apid/ssc/date in sec since 1970
    static int nb_apid_stored = 0;

    int found_hole = 0;

    int i;
    for (i = 0; i < NB_APID; i++) {
        if (apid_ssc[i][0] == apid) {
            if (apid_ssc[i][1]+1 != ssc ) {
                if (ssc == 0 )
                    printf("APID %d Last ssc %d, next zero, assuming restart\n", apid, apid_ssc[i][1]);
                else
                {
                    if (ssc - apid_ssc[i][1] == 0)
                        printf("Warning: the next SSC is equal to the previous SSC, it happens sometimes in EDDS responses, the packet seems to be duplicated, no missing here.");
                    else
                    {
                        char start_date[50], end_date[50], str_date[50];
                        strftime(str_date, sizeof(str_date), "%Y%m%dT%H:%M:%SZ", ts);
                        if(argc < 3){
                            printf("APID %d Last ssc %d, new ssc %d MISSING %d packet generation date %s\n", apid, apid_ssc[i][1], ssc, (ssc-apid_ssc[i][1]-1), str_date);
                        }
                        time_t sec_start, sec_end;
                        struct tm ts_start, ts_end;
                        sec_start = apid_ssc[i][2];
                        sec_end = sec_pkt;
                        ts_start = *gmtime(&sec_start);
                        ts_end = *gmtime(&sec_end);

                        strftime(start_date, sizeof(start_date), "%Y%m%dT%H:%M:%SZ", &ts_start);
                        strftime(end_date, sizeof(end_date), "%Y%m%dT%H:%M:%SZ", &ts_end);

                        add_missing(missings, apid, sec_start, sec_end);
                        if(argc > 2){
                            if(strcmp(argv[2],"-missing") == 0){
			      // as we add the previous file and the following one to catch holes across boundary keep only holes
			      // if date in the filename (./hk_2020-03-03.xml) and start_date are identical
			      int nb_missing;
			      nb_missing = (ssc-apid_ssc[i][1]-1);
			      // if negative add 65536 to return to positive
			      if (nb_missing < 0) nb_missing += 65536;
			      char an[5], mois[3],jour[3],*p;
			      p = strchr(name_file,'_');
			      if ( p != NULL)  {
				strncpy(an,p+1,4);an[4]='\0';
				strncpy(mois,p+6, 2);mois[2]='\0';
				strncpy(jour,p+9, 2);jour[2]='\0';
				fprintf(stderr,"fichier %s ---an %s -- mois %s -- jour %s start_date %s\n",name_file,an,mois,jour,start_date);
				int a = atoi(an); int m = atoi(mois);int j = atoi(jour);
				if (a>2000 && a<2070 && m>=0 && m <13 && j>0 && j <32) {
				  if (strncmp(an,start_date,4)==0 && strncmp(mois,start_date+4,2)==0 && strncmp(jour,start_date+6,2)==0) {
				    fprintf(file_log_unfound,"%s\t%s\t%s\t%d\t%d\t%d\t%d\t%d\t%d\n",name_file,start_date,end_date,apid,type,subtype,apid_ssc[i][1],ssc,nb_missing);
				    found_hole = 1;
				  }
				  else {
				    fprintf(stderr,"on passe pas la ou je crois 1\n");
				  }
				}
				else { // out naming convention
				  fprintf(file_log_unfound,"%s\t%s\t%s\t%d\t%d\t%d\t%d\t%d\t%d\n",name_file,start_date,end_date,apid,type,subtype,apid_ssc[i][1],ssc,nb_missing);
				}
			      }
			      else { // out naming convention
				fprintf(file_log_unfound,"%s\t%s\t%s\t%d\t%d\t%d\t%d\t%d\t%d\n",name_file,start_date,end_date,apid,type,subtype,apid_ssc[i][1],ssc,nb_missing);
			      }
                            }
                        }
                    }
                }
            }
            apid_ssc[i][1]= ssc;
            apid_ssc[i][2]= sec_pkt;
            break;
        }
    }

    if ( i == NB_APID )  {   // apid was not found (i is from previous loop)
        apid_ssc[nb_apid_stored][0] = apid;
        apid_ssc[nb_apid_stored][1] = ssc;
        nb_apid_stored ++;
        if (nb_apid_stored >= NB_APID) {
            printf("error, nb of apid too small\n");
            exit(1);
        }
    }
    return found_hole;
}

int read_packet(char* packet_str, Missings* missings,char * name_file, FILE* file_log_unfound,int argc,char** argv)
{
    int apid, type, subtype, ssc;
    char* date;
    char s_str_time[9];  // a number of 32 bit, coded in hexa in ascii takes 4*8 char
    char type_str[3];
    char subtype_str[3];
    char str_apid[5];
    char str_ssc[5];
    int apid_without_mask, ssc_without_mask;
    short apid_mask = 2047; // 0000011111111111
    short ssc_mask = 16383; // 0011111111111111
    time_t s_time_in_secs;

    // filling  time
    memcpy(s_str_time, packet_str + 8, 8);
    s_str_time[8]='\0';
    s_time_in_secs = (time_t)strtol(s_str_time, NULL, 16);
    date = ctime(&s_time_in_secs);
    date[strlen(date) - 1] = '\0';

    // creation time corresponding to storage time
    char s2_str_time[9];
    time_t s2_time_in_secs;
    char *date2;
    memcpy(s2_str_time, packet_str + 24, 8);
    s2_str_time[8]='\0';
    s2_time_in_secs = (time_t)strtol(s2_str_time, NULL, 16);
    date2 = ctime(&s2_time_in_secs);
    date2[strlen(date) - 1] = '\0';

    // read size, total len of packet in bytes, ie 2 char in the xml file
    char str_size[9]; // 32 bits
    int len;
    memcpy(str_size, packet_str + 56, 8);
    str_size[8]='\0';
    len = (int) strtol(str_size, NULL, 16);


    // read type
    memcpy(type_str, packet_str + 148, 2);
    type_str[2] = '\0';
    type = (int)strtol(type_str, NULL, 16);

    // read sub type
    memcpy(subtype_str, packet_str + 150, 2);
    subtype_str[2] = '\0';
    subtype = (int)strtol(subtype_str, NULL, 16);

    // apid
    memcpy(str_apid, packet_str + 140, 4);
    str_apid[4] = '\0';
    apid_without_mask = (short)strtol(str_apid, NULL, 16);
    apid = apid_without_mask & apid_mask;

    // ssc
    memcpy(str_ssc, packet_str + 144, 4);
    str_ssc[4]='\0';
    ssc_without_mask = (short)strtol(str_ssc, NULL, 16);
    ssc = ssc_without_mask & ssc_mask;

    // trying to get generation date from inside pus packet
    char str_time[9];
    long int secondes;
    memcpy(str_time, packet_str+ 152  + 20, 8); // buff +1 : decalage de 4 bits, buff +2 : decalage de 1 octet
    str_time[8] = '\0';
#define GMT_EPOCH_2000_DIFF 946684800
    // fprintf(stderr, "\n    2)\n    sec in hexa is %s", str_time);
    long int secondes_pkt;
    secondes_pkt = (time_t)((unsigned long int)(strtol(str_time, NULL, 16) ));
    secondes = (time_t)((unsigned long int)(strtol(str_time, NULL, 16) + GMT_EPOCH_2000_DIFF));
    struct tm ts;
    char  str_date[80];
    time_t truc = secondes;
    ts = *gmtime(&truc);
    strftime(str_date, sizeof(str_date), "%Y%m%dT%H:%M:%SZ", &ts);
    if(argc < 3){
      printf("scos %s \tgeneration date %s secondes %ld len(byte, 2 char) %d\t%d\t%d\t%d\t%d\n", date, str_date, secondes_pkt , len, apid, ssc, type, subtype);
    }
    int found_hole = check_ssc(apid,ssc, &ts, truc, missings,name_file,type,subtype,file_log_unfound,argc,argv);
    return(found_hole);
}

int main(int argc, char** argv)
{
  FILE *file_log_unfound=NULL;
  int found_hole = 0; // false
  if (argc < 2)
    {
      fprintf(stderr,"You need to give the input file path in argument: ./unpack_scos input_file.xml [-missing logfile] \n");
      exit(1);
    }
  else
    {
      if(argc > 2){
	if (argc > 3 && strcmp(argv[2],"-missing") == 0 ){
	  char chemin[1024];

	  strcpy(chemin,argv[3]);
	  file_log_unfound = fopen(chemin, "r");

	  if (file_log_unfound == NULL) {/* file empty */
	    file_log_unfound = fopen(chemin, "a");
	    fprintf(file_log_unfound,"PATH file\tstart_date_gap\tend_date_gap\tAPID\ttype\tsubtype\tLast_ssc\tNew_ssc\tMISSING\n");
	  }
	  else file_log_unfound = fopen(chemin, "a");
	  // file opened
	}
	else {
	  fprintf(stderr,"You need to give the input file path in argument: ./unpack_scos input_file.xml [-missing logfile] \n");
	  exit(1);
	}
      }
      
      xmlDoc *document;
      xmlNode *root, *node, *response, *pkt_list, *pkt, *raw_pkt;
      char* str_num;
      char* current_packet;
      int size_to_alloc;

      Missings *missings = (Missings*)malloc(sizeof(Missings));
      missings->gaps = NULL;
      missings->nb_gaps = 0;
      missings->apids = NULL;
      missings->nb_apids = 0;

      document = xmlReadFile(argv[1], NULL, 0);
      root = xmlDocGetRootElement(document);
      for (node = root->children; node; node = node->next) {
	if (strcmp((char*)(node->name) , "Response")==0) {
	  response = node;
	  for (pkt_list = response->children; pkt_list;pkt_list = pkt_list->next) {
	    if (strcmp((char*)(pkt_list->name), "PktRawResponse")==0) {
	      pkt = pkt_list;
	      for (pkt = pkt_list->children;  pkt; pkt = pkt->next) {
		str_num = (char*)xmlGetProp(pkt, (xmlChar*)"packetID");
		if (str_num != NULL && argc < 3)
		  printf("%s\t", str_num);
		  
		for (raw_pkt = pkt->children; raw_pkt; raw_pkt = raw_pkt->next)
		  {
		    if (strcmp((char*)(raw_pkt->name), "Packet") == 0)
		      {
			size_to_alloc = strlen((char*)xmlNodeGetContent(raw_pkt)) + 1;
			current_packet = (char*)malloc(sizeof(char)*size_to_alloc);
			if(argc < 3)
			  printf("size xml %d\t", size_to_alloc);
			strcpy(current_packet, (char*)xmlNodeGetContent(raw_pkt));
			int rtc = read_packet(current_packet, missings,argv[1],file_log_unfound,argc,argv);
			if (rtc == 1 && found_hole == 0) found_hole = 1;
			// printf("len in byte %ld PUS %s\n", strlen(current_packet)/2, current_packet + 152);
			free(current_packet);
		      }
		  }
	      }
	    }
	  }
	}
      }

      int i, j, current_apid;
      Gap current_gap;
      current_gap.start_sec = 0;
      Gap previous_gap;
      Gap edited_gap; // the gap between start and end will potentially increase
      short int printed;

      for (i = 0; i < missings->nb_apids; i++)
        {
	  current_apid = (missings->apids)[i];
	  current_gap.apid = current_apid;
	  previous_gap.apid = current_apid;
	  previous_gap.start_sec = 0;
	  edited_gap.apid = current_apid;
	  if(argc != 3 || (strcmp(argv[2],"-missing") != 0 )){
	    for (j = 0; j < missings->nb_gaps; j++)
	      {
		current_gap = (missings->gaps)[j];
		
		if (current_apid == current_gap.apid)
		  {
		    printed = 0;
		    
		    if (previous_gap.start_sec != 0) // otherwise it's the first iterance: printed == 0
		      {
			if (current_gap.start_sec - previous_gap.end_sec > MERGE_MISSING_LIMIT)
			  {
			    printed = 1;
			    print_edds_requests(edited_gap);
			    edited_gap.start_sec = current_gap.start_sec;
			    edited_gap.end_sec = current_gap.end_sec;
			  }
		      }
		    else
		      edited_gap.start_sec = current_gap.start_sec;

		    if (!printed) edited_gap.end_sec = current_gap.end_sec;

		    previous_gap.start_sec = current_gap.start_sec;
		    previous_gap.end_sec = current_gap.end_sec;
		  }
	      }

            // Don't forget the last ones:
	    print_edds_requests(edited_gap);
	  }
	  
        }
      if(argc > 2){
	if(strcmp(argv[2],"-missing") == 0){
	  if (found_hole == 0) {
	    fprintf(file_log_unfound,"%s\t%s\t%s\t%d\t%d\t%d\t%d\t%d\t%d\n",argv[1],"-","-",0,0,0,0,0,0);
	    fprintf(stderr,"file no hole %s \n",argv[1]);
	  }
	  else fprintf(stderr,"file with hole %s ",argv[1]);
	  fclose(file_log_unfound);
	}
      }
	    
      free(missings->apids);
      free(missings->gaps);
      free(missings);

      return EXIT_SUCCESS;
    }
}
