# check if expected science has arrived
set -x
scp nb@spice-nb.ias.u-psud.fr:/archive/SOLAR-ORBITER/SPICE/fits/spice_missing_files.csv .
awk -F "," '{print $1}' spice_missing_files.csv | sort -u | grep -v DATE > dates.txt
DEBUT=2022-06-07
FIN=$(date --date='-30 day ' '+%Y-%m-%d')
echo checking range $DEBUT to $FIN

for dat in `cat dates.txt`
do
    if [ $(date -d $dat +%s) -lt $(date -d $DEBUT +%s) ]
    then
       echo ignoring $dat
    else
	if [ $(date -d $dat +%s) -gt $(date -d $FIN +%s) ]
	then
	    echo ignoring $dat
	else 
	    echo querying science packet for day  $dat
	    datt=${dat}T00:00:00
	    echo datt $datt
	    deb=$(date -d "$datt" '+%Y%m%dT%H:%M:%SZ')
	    fin=$(date -d "$datt +1 day + 1 hour +10 min" '+%Y%m%dT%H:%M:%SZ')
	    # prepare edds request
	    /home/edds/.local/bin/edds -a '1388, 1404, 1644, 1628'  -s ${deb} -e ${fin} --config /home/edds/spice_config.json -f prefix -r retriv_$$_${dat} -q -b PARC_FLIGHT_SOL -g 
	    # sometime edds is not finished sending data
	    sleep 3600
	    for f in `ls -1 /home/edds/edds_response/retriv_$$_${dat}* `
	    do
		/opt/merge_xml/insert_xml $f sc
	    done
	fi
    fi
done
