#!/bin/bash

# Print the EDDS requests for specific params (decoded xml packets given)
# Give a date to define another start (than the beginning of the mission). The end date is today excluded.

#root_path='/home/david/projects/unpack_scos/'
root_path='/home/edds/unpack_scos/'
gap_in_days=7 # to split requests instead of a big one
params='NPWT2519,NPWT2503,NTCD50L6,NTCD4ZZ6,NRU02615,NRU02609,NCFT1C80'
apids='212,260'
prefix_resp='david_with_apid' # prefix to put into the edds response file
sleep_value=300 # how many seconds does it have to wait between requests
requests_file='process_specific_params_wa.sh' # file which will contain the requests to run (need to be checked first)

if [ -n "$1" ]
then
	date -d $1 +'%Y-%m-%d' >/dev/null 2>&1
	if [ ! $? -eq 0 ]
	then
		echo 'The argument has to be in the YYYY-MM-DD format.'
		exit
	fi

	start_date=$1
else
	start_date='2020-01-29' # "2020-01-29": the first date with telemetry in /home/edds/edds_response
fi

today_str=$(date '+%Y-%m-%d')
> $requests_file
current_date=$start_date
while [[ "$current_date" < "$today_str" || "$current_date" == "$today_str" ]]
do
	startReq="${current_date//-/}"
	current_date=$(date +%Y-%m-%d -d $current_date' + '$gap_in_days' day')
	if [[ "$current_date" > "$today_str" ]]
	then
		endReq="${today_str//-/}"
	else
		endReq="${current_date//-/}"
	fi
	if [[ "$startReq" != "${start_date//-/}" ]]
	then
		echo 'echo -e "\nSleeping '$sleep_value's between requests..."' >> $requests_file
		echo 'sleep '$sleep_value >> $requests_file
	fi
	echo 'python3 -m edds_client.cli --config ../spice_config.json -a '$apids' -f prefix -r '$prefix_resp' -s '$startReq'T00:00:00Z -e '$endReq'T00:00:00Z --data_type Param --param_filter '$params' --data_space DARC' >> $requests_file
done

echo -e 'Done.\nRequests are stored in '$requests_file'\nNow, check this script content, then run it from edds@spice-gfts:/home/edds/edds_client/\nResponse will be in the files: /home/edds/edds_response/'$prefix_resp'*'
