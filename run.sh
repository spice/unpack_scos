#!/bin/bash

if [ -n "$1" ]
then
	input_path=$1
else
	input_path='input_file.txt'
fi

rm -f *.o
rm unpack_scos
fail=0
make || fail=1
rm -f *.o
if [ $fail == 0 ]
then
	command='./unpack_scos '$input_path
	echo -e 'Doing: '$command'\n'
	$command
fi
