#!/bin/bash
# If an argument is given (YYYY-MM-DD) this date is used instead of today

#root_path='/home/david/projects/unpack_scos/'
root_path='/home/edds/unpack_scos/'

nb_days=3 # check nb days in the past

offset_days=2
# check in the past: 0 = from today (or argument) ; 1 = from yesterday (or argument - 1 day)...
# 3 and 2 makes: j-2, j-3, j-4

past_day_offset="$(($nb_days+$offset_days-1))"

log_path=$root_path'missing_logs'
log_folder='logs_manually'
today_str=$(date '+%Y-%m-%d')
log_date=$today_str
log_name='edds_requests_'
date_to_use='today'
was_run_subfolder='manually'

if [ -n "$1" ]
then
	if [ $1 = 'cron' ]
	then
		was_run_subfolder='cron'
		log_folder='logs_cron'
		log_date=$today_str
	else
		date -d $1 +'%Y-%m-%d' >/dev/null 2>&1
		if [ ! $? -eq 0 ]
		then
			echo 'The argument has to be in the YYYY-MM-DD format.'
			exit
		fi

		if [ -n "$2" ]
		then
			if [ $2 = 'replay' ] # from replay
			then
				was_run_subfolder='replay'
				log_folder='logs_replay'
			else
				was_run_subfolder='manually'
				log_folder='logs_manually'
			fi

			log_date=$1
		fi
		date_to_use=$1
	fi
fi

past_day=$(date -d $date_to_use' - '$past_day_offset' days' +'%Y-%m-%d')
final_log=$log_path'/'$log_folder'/'$log_name$log_date'.log'

next_day=$past_day

>$final_log
rm -f $log_path'/'$log_folder'/merged.xml'
rm -rf $log_path'/'$log_folder'/files_to_merge'
mkdir -p $log_path'/'$log_folder'/files_to_merge'

for i in `seq 1 $nb_days`
do
	dates_str=$dates_str' '$next_day
	next_day_y=$(date -d $next_day +'%Y')
	next_day_m=$(date -d $next_day +'%m')
	next_day_d=$(date -d $next_day +'%d')
	telemetry_path='/telemetry/'$next_day_y'/'$next_day_m'/'$next_day_d

	to_print='\n==> '$telemetry_path': '
	if [ -d $telemetry_path ]
	then
		for xml_file in `ls $telemetry_path`
		do
			to_print=$to_print'\n'$xml_file'... '
			cp $telemetry_path'/'$xml_file $log_path'/'$log_folder'/files_to_merge'
		done
	else
		to_print=$to_print'no XML file found (this message is fine)'
	fi
	echo -e $to_print

	next_day=$(date +%Y-%m-%d -d $next_day' + 1 day')
done

$root_path'../merge_xml/merge_xml' $log_path'/'$log_folder'/files_to_merge' $log_path'/'$log_folder'/merged.xml'
$root_path'unpack_scos' $log_path'/'$log_folder'/merged.xml' >/dev/null 2>$final_log

rm -rf $log_path'/'$log_folder'/files_to_merge'
rm -f $log_path'/'$log_folder'/merged.xml'

echo -e '\n==> To sum up:'
sum_up_content=$(cat $final_log)
echo -e '\nLog file created: '$final_log'\n'
if [[ $sum_up_content == '' ]]
then
	no_missing_msg='No missing packet found for '$dates_str'.'
	echo -e '\n'$no_missing_msg
	echo $no_missing_msg >$final_log
else
	cat $final_log

	proceed=false
	if [ -n "$2" ] && [ $2 = 'replay' ]
	then
		proceed=false
	elif [ -n "$2" ] && [ $2 = 'Y' ]
	then
		proceed=true
	elif [ -n "$2" ] && [ $2 = 'N' ]
	then
		proceed=false
	else
		echo -e '\nYou can give Y or N (to check_missing_packets.sh) as a second argument to proceed (or not) avoiding this question:\nDo you want to proceed the requests above and fill /telemetry as well? [Y/n] ("retriv" will be changed)'
		read user_answer
		if [ $user_answer = 'y' ] || [ $user_answer = 'Y' ]
		then
			proceed=true
		fi
	fi

	if [ $proceed = true ]
	then
		echo -e '\n\nProcessing the requests and filling /telemetry...'
		$root_path'process_missing_packets.sh' $final_log $was_run_subfolder
	else
		echo -e '\nThe EDDS requests and insert_xml (to fill /telemetry) were not proceeded.\nTo do it, from unpack_scos, type:\n./process_missing_packets.sh '$final_log
	fi
fi
